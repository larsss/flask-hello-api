from flask_restful import Resource
from flask import request
from flask import current_app

class Hello(Resource):

    def get(self, hello_id):
        print(hello_id)
        return {hello_id: current_app.hellos[str(hello_id)]}

    def put(self, hello_id):
        hellos[hello_id] = request.form['data']
        return {hello_id: hellos[hello_id]}

class HelloList(Resource):

    def get(self):
        return current_app.hellos
    
