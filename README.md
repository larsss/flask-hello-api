## Hello API with scheduler ##

```
#!bash
python3 -m venv venv
source venv/bin/activate
flask run
```



```
http://127.0.0.1:5000/hellos
http://127.0.0.1:5000/hello/1
http://127.0.0.1:5000/hello/2
```
