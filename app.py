import os
import time
import logging

from flask import Flask
from flask_restful import Api
from flask_apscheduler import APScheduler

from resources.hello import Hello, HelloList


logger = logging.getLogger()
logger.setLevel(level=os.environ.get("LOGLEVEL", "INFO"))


hellos = {
    "1":"hello1",
    "2":"hello2"
}

counter = 2

def job1():
    global counter
    counter = counter + 1

    hellos[str(counter)] = "hello{}".format(counter)
    
    print(counter)
    time.sleep(5)


def create_app():
    global hellos
    
    app = Flask(__name__)
    #app.config.from_object(Config())
    app.hellos = hellos

    scheduler = APScheduler()
    scheduler.init_app(app)
    app.apscheduler.add_job(func=job1, id='job1', trigger='interval', seconds=10)
    scheduler.start()

    api = Api(app)
    api.add_resource(Hello, '/hello/<int:hello_id>')
    api.add_resource(HelloList, '/hellos')
    
    return app

app = create_app()


@app.route("/")
def hey():
    print(counter)
    return str(counter)


